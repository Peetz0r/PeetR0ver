#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <WiFiUdp.h>
#include <EEPROM.h>

#define PIN_FORWARD 5
#define PIN_BACK    4
#define PIN_LEFT   13
#define PIN_RIGHT  12

#define WIFI_HTM_PROGMEM

WiFiUDP udp;
unsigned int localUdpPort = 42;

unsigned long timeOfKeepalive = 0;
unsigned long timeOfBroadcast = 0;

uint16_t value;

bool duobleResetWaiting = true;

void allStop() {
  digitalWrite(PIN_FORWARD, 0);
  digitalWrite(PIN_BACK,    0);
  digitalWrite(PIN_LEFT,    0);
  digitalWrite(PIN_RIGHT,   0);
}

void forward(uint16_t n) {
  if(n>0) back(0);
  analogWrite(PIN_FORWARD, n);
}

void back(uint16_t n) {
  if(n>0) forward(0);
  analogWrite(PIN_BACK, n);
}

void left(uint16_t n) {
  if(n>0) right(0);
  analogWrite(PIN_LEFT, n);
}

void right(uint16_t n) {
  if(n>0) left(0);
  analogWrite(PIN_RIGHT, n);
}

void coast() {
  analogWrite(PIN_FORWARD, 0);
  analogWrite(PIN_BACK,    0);
}

void straight() {
  analogWrite(PIN_LEFT,    0);
  analogWrite(PIN_RIGHT,   0);
}

void setup() {
  delay(1000);
  Serial.begin(115200);
  Serial.println("Start");

  WiFiManager wifiManager;

  analogWriteFreq(100);
  pinMode(PIN_FORWARD, OUTPUT);
  pinMode(PIN_BACK,    OUTPUT);
  pinMode(PIN_LEFT,    OUTPUT);
  pinMode(PIN_RIGHT,   OUTPUT);
  allStop();

  EEPROM.begin(4);
  bool doubleReset = EEPROM.read(0) == 0x42;
  EEPROM.write(0, 0x42);
  EEPROM.commit();

  if(doubleReset) {
    Serial.println("Double Reset Detected");
    duobleResetWaiting = false;
    EEPROM.write(0, 0);
    EEPROM.end();
    wifiManager.startConfigPortal("PeetR0ver", "h4zenberg");
  } else {
    Serial.println("No Double Reset Detected");
    wifiManager.autoConnect("PeetR0ver", "h4zenberg");
  }

  udp.begin(localUdpPort);
  Serial.printf("UDP server started on port %d\n", localUdpPort);
}

void loop() {
  if(duobleResetWaiting && millis() > 5000) {
    duobleResetWaiting = false;
    Serial.println("Double Reset timeout");
    EEPROM.write(0, 0);
    EEPROM.end();
  }

  char incomingPacket[32];
  int packetSize = udp.parsePacket();
  if (packetSize) {
    int len = udp.read(incomingPacket, 32);
    if (len > 0) {
      incomingPacket[len] = 0;
    }

    timeOfKeepalive = millis();

    char *p = incomingPacket;
    char *cmdName = strsep(&p, " ");
    char *cmdTimeS = strsep(&p, " ");
    char *cmdValueS = strsep(&p, " ");

    uint16_t cmdValue = 0;

    if (cmdTimeS != NULL) {
      long timeDiff = millis() - atoi(cmdTimeS);

      udp.beginPacket(IPAddress(255,255,255,255), 42424);
      udp.write(incomingPacket);
      udp.write("\n");

      char buf[32];
      itoa(timeDiff, buf, 10);
      udp.write(buf);

      udp.endPacket();

      if (timeDiff < 0) {
        Serial.printf("time diff invalid, are you timetraveling? %lums\n", timeDiff);
        return;
      } else if (timeDiff > 200) {
        Serial.printf("time diff invalid, you are too slow; %lums\n", timeDiff);
        return;
      } else {
        Serial.printf("time diff OK: %lums\n", timeDiff);
      }
    } else {
      Serial.println("Invalid packet without timestamp:");
      Serial.println(incomingPacket);
      allStop();
      timeOfKeepalive = 0;
      return;
    }
    if (cmdValueS != NULL) {
      cmdValue = atoi(cmdValueS);
    }

           if (strcmp(cmdName, "stop") == 0) {
      allStop();
    } else if (strcmp(cmdName, "forward") == 0) {
      forward(cmdValue);
    } else if (strcmp(cmdName, "back") == 0) {
      back(cmdValue);
    } else if (strcmp(cmdName, "left") == 0) {
      left(cmdValue);
    } else if (strcmp(cmdName, "right") == 0) {
      right(cmdValue);
    } else if (strcmp(cmdName, "coast") == 0) {
      coast();
    } else if (strcmp(cmdName, "straight") == 0) {
      straight();
    } else {
      Serial.println("Unknown packet:");
      Serial.println(incomingPacket);
      allStop();
      timeOfKeepalive = 0;
      return;
    }
  }

  if (timeOfKeepalive > 0 && millis() > timeOfKeepalive + 50) {
    allStop();
    timeOfKeepalive = 0;
    Serial.println("Did not get keepalive!");
  }

  if (millis() > timeOfBroadcast + 1000) {
    timeOfBroadcast = millis();
    udp.beginPacket(IPAddress(255,255,255,255), 4242);
    udp.write("PeetR0ver ");

    char buf[32];
    String(millis(), DEC).toCharArray(buf, 32);
    udp.write(buf);

    udp.endPacket();
    Serial.println("Sent broadcast packet!");
  }
}
