import machine, network, usocket

pwms = []

def forward(n):
  if n>0:
    back(0)
  pwms[1].duty(n)

def back(n):
  if n>0:
    forward(0)
  pwms[0].duty(n)

def left(n):
  if n>0:
    right(0)
  pwms[3].duty(n)

def right(n):
  if n>0:
    left(0)
  pwms[2].duty(n)

def coast():
  pwms[0].duty(0)
  pwms[1].duty(0)

def straight():
  pwms[2].duty(0)
  pwms[3].duty(0)

def all_stop():
  for p in pwms:
    p.duty(0)

def main():

  for i in [4, 5, 12, 13]:
    pwms.append(machine.PWM(machine.Pin(i, machine.Pin.OUT), freq=100, duty=0))

  s = usocket.socket()
  s.setsockopt(usocket.SOL_SOCKET, usocket.SO_REUSEADDR, 1)
  s.bind(usocket.getaddrinfo('0.0.0.0', 42)[0][-1])
  s.listen(5)
  print('Server listening')

  while True:
    res = s.accept()
    client_sock = res[0]

    print('connection accepted')

    while True:
      try:
        line = client_sock.readline().decode()
        print(line)
        if 'forward' in line:
          value = int(line.split(' ')[1])
          forward(value)
        elif 'back' in line:
          value = int(line.split(' ')[1])
          back(value)
        elif 'left' in line:
          value = int(line.split(' ')[1])
          left(value)
        elif 'right' in line:
          value = int(line.split(' ')[1])
          right(value)
        elif('coast') in line:
          coast()
        elif('straight') in line:
          straight()
      except Exception as e:
        print(e)

main()
