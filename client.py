#!/bin/env python3

import pygame, socket, time
from math import isclose

host = None
PORT = 42
millis = 0

def send(msg):
  if(host != None):
    try:
      s.sendto(msg, (host, PORT))
    except ConnectionRefusedError as e:
      pass

pygame.init()
j = pygame.joystick.Joystick(0)
j.init()

print('Detected joystick: %s' % j.get_name())

if j.get_name() == 'Nintendo Wii Remote in gamepad mode':
  j_conf = [(0, 'forward', 'coast', 'back'), (1, 'right', 'straight', 'left')]
elif j.get_name() == 'Mega World Thrustmaster dual analog 3.2':
  j_conf = [(1, 'forward', 'coast', 'back'), (2, 'left', 'straight', 'right')]
elif j.get_name() == 'Wireless Steam Controller':
  j_conf = [(1, 'forward', 'coast', 'back'), (0, 'left', 'straight', 'right')]

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind(('0.0.0.0', 4242))
s.setblocking(False)
while True:
  try:
    dgram = s.recvfrom(32)
    if dgram[0].startswith(b'PeetR0ver'):
      millis = int(dgram[0].split(b' ')[1])
      if host != dgram[1][0]:
        host = dgram[1][0]
        print('found at %s' % host)
  except OSError as e:
    pass

  pygame.event.pump()

  for i in j_conf:
    value = int(max(-1023, min(1023, j.get_axis(i[0])*1024)))
    cmd = i[int(max(-1, min(1, j.get_axis(i[0])*1024)))+2]
    msg = '%s %d %d' % (cmd, millis, abs(value))
    print(msg)
    send(msg.encode())

  millis += 25
  time.sleep(0.025)
