import 'dart:async';
import 'dart:math';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:sensors/sensors.dart';
import 'package:collection/collection.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'PeetR0ver',
      theme: new ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: new MyHomePage(title: 'PeetR0ver'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _accelerationEnabled = false;
  List<double> _accelerometerValues;
  List<StreamSubscription<dynamic>> _streamSubscriptions = <StreamSubscription<dynamic>>[];

  bool _touchEnabled = false;

  List<String> _log = [];
  List<String> _msg = [null, null, null, null];
  Timer duinoMillisTimer;
  int duinoMillis = 0;

  RawDatagramSocket _socket;
  InternetAddress _host;
  int _port = 42;

  @override
  void dispose() {
    super.dispose();
    for (StreamSubscription<dynamic> subscription in _streamSubscriptions) {
      subscription.cancel();
    }
  }

  @override
  void initState() {
    super.initState();
    RawDatagramSocket.bind(InternetAddress.anyIPv4, 4242).then((RawDatagramSocket socket) {
      _socket = socket;
    });

    duinoMillisTimer = new Timer.periodic(new Duration(milliseconds: 25), handleDuinoMillisTimer);

    _streamSubscriptions.add(accelerometerEvents.listen((AccelerometerEvent event) {
      setState(() {
        _accelerometerValues = <double>[event.x, event.y, event.z];
      });
    }));
  }

  void handleDuinoMillisTimer(Timer t) {
    duinoMillis += 25;

    if (_socket != null) {
      Datagram dgram;
      while ((dgram = _socket.receive()) != null) {
        List<String> dgramFields = String.fromCharCodes(dgram.data).split(' ');
          if (dgramFields[0] == 'PeetR0ver') {
          duinoMillis = int.parse(dgramFields[1]);
          if (dgram.address != _host) {
            _log.add('found at ${dgram.address.address}');
            _host = dgram.address;
          }
        }
      }
    }

    send();
  }

  void allStop() {
    _log.add('stop!');
    if (_host != null) {
      _socket.send('stop $duinoMillis'.codeUnits, _host, _port);
      _msg = [null, null, null, null];
    }
  }

  void send([Timer t]) {
    if (_host != null) {
      for (int i in [0, 2]) {
        if (_msg[i] != null) {
          if (_msg[i + 1] != null) {
            _socket.send('${_msg[i]} $duinoMillis ${_msg[i + 1]}'.codeUnits, _host, _port);
          } else {
            _socket.send('${_msg[i]} $duinoMillis'.codeUnits, _host, _port);
          }
        }
      }
    }
  }

  void doTouch(var details) {
    if (_touchEnabled) {
      RenderBox getBox = context.findRenderObject();
      num touchX = details.globalPosition.dx / getBox.constraints.minWidth * 20 - 10;
      num touchY = (details.globalPosition.dy - 80) / (getBox.constraints.minHeight - 80) * 20 - 10;

      _msg[0] = 'straight';
      _msg[1] = null;
      if (touchX.abs() > 1) {
        _msg[0] = (touchX < 0 ? 'left' : 'right');
        _msg[1] = min(1023, (touchX.abs() - 1) / 7 * 1024).toStringAsFixed(0);
      }

      _msg[2] = 'coast';
      _msg[3] = null;
      if (touchY.abs() > 1) {
        _msg[2] = (touchY < 0 ? 'forward' : 'back');
        _msg[3] = min(1023, (touchY.abs() - 1) / 5 * 1024).toStringAsFixed(0);
      }

      _log.add('${_msg[0]} ${_msg[1] ?? ''}        ${_msg[2]} ${_msg[3] ?? ''}');
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_accelerationEnabled) {
      _msg[0] = 'straight';
      _msg[1] = null;
      if (_accelerometerValues[0].abs() > 1) {
        _msg[0] = (_accelerometerValues[0] > 0 ? 'left' : 'right');
        _msg[1] = min(1023, (_accelerometerValues[0].abs() - 1) / 4 * 1024).toStringAsFixed(0);
      }

      _msg[2] = 'coast';
      _msg[3] = null;
      if ((_accelerometerValues[1] - 5).abs() > 1) {
        _msg[2] = (_accelerometerValues[1] < 5 ? 'forward' : 'back');
        _msg[3] = min(1023, ((_accelerometerValues[1] - 5).abs() - 1) / 3 * 1024).toStringAsFixed(0);
      }

      _log.add('${_msg[0]} ${_msg[1] ?? ''} ${_msg[2]} ${_msg[3] ?? ''}');
    }

    if (_log.length > 25) {
      _log.removeRange(0, _log.length - 25);
    }

    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: new GestureDetector(
        onPanStart: doTouch,
        onPanDown: doTouch,
        onPanUpdate: doTouch,
        onPanEnd: (x) => allStop(),
        onPanCancel: () => allStop(),
        child: new Container(
          color: Colors.yellowAccent,
          child: new Stack(
            children: <Widget>[
              _touchEnabled
                  ? new Positioned.fill(
                      child: new Divider(color: Colors.deepPurple),
                    )
                  : new Container(),
              _touchEnabled
                  ? new Positioned.fill(
                      child: new RotatedBox(
                        quarterTurns: 1,
                        child: new Divider(color: Colors.deepPurple),
                      ),
                    )
                  : new Container(),
              new Center(
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        new Text('Touch position'),
                        new Switch(
                            value: _touchEnabled,
                            onChanged: ((v) {
                              _touchEnabled = v;
                              if (!v) {
                                allStop();
                              }
                            })),
                      ],
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        new Text('Acceleration Sensor'),
                        new Switch(
                            value: _accelerationEnabled,
                            onChanged: ((v) {
                              _accelerationEnabled = v;
                              if (!v) {
                                allStop();
                              }
                            })),
                      ],
                    ),
                    new Text(
                      _log.join('\n'),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: new FloatingActionButton(
        child: new Icon(Icons.stop),
        backgroundColor: Colors.red,
        onPressed: () {
          allStop();
          _accelerationEnabled = _touchEnabled = false;
        },
      ),
    );
  }
}
